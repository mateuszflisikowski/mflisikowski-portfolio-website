/* eslint-disable global-require */
module.exports = {
  MiniCssExtractPlugin: require('mini-css-extract-plugin'),
  CleanWebpackPlugin: require('clean-webpack-plugin').CleanWebpackPlugin,
  HtmlWebpackPlugin: require('html-webpack-plugin'),
  TodoWebpackPlugin: require('todo-webpack-plugin'),
  OptimizeCssAssetsPlugin: require('optimize-css-assets-webpack-plugin'),
  TerserPlugin: require('terser-webpack-plugin')
}
