import merge from 'webpack-merge'
import config from './webpack.config.commons'
import * as plugs from './webpack.config.plugins'

const rules = [
  {
    test: /\.(sa|sc|c)ss$/,
    use: [plugs.MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader']
  }
]

const plugins = [
  new plugs.OptimizeCssAssetsPlugin({
    cssProcessorOptions: {
      map: {
        inline: false,
        annotation: true
      }
    }
  }),

  new plugs.TerserPlugin({
    // Use multi-process parallel running to improve the build speed
    // Default number of concurrent runs: os.cpus().length - 1
    parallel: true,
    // Enable file caching
    cache: true,
    sourceMap: true
  }),

  new plugs.MiniCssExtractPlugin({
    chunkFilename: '[id].css',
    filename: '[name].css'
  })
]

module.exports = merge(config, {
  mode: 'production',

  devtool: 'source-map',

  module: {
    rules
  },

  plugins
})
