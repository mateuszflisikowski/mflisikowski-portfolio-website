/* Output directory path */
export const WEBPACK__PATH__BUILD = '../public'

/* Source directory path */
export const WEBPACK__PATH__SRC = '../src'

/* Public path */
export const WEBPACK__PATH__PUBLIC = '/'

/* All general directories path */
export const WEBPACK__PATHS = {
  HANDLEBARS: `${WEBPACK__PATH__SRC}/handlebars`,
  JAVASCRIPT: `${WEBPACK__PATH__SRC}/javascript`,
  STYLESHEET: `${WEBPACK__PATH__SRC}/stylesheet`
}

/* All paths for handlebars */
export const WEBPACK__PATH__HANDLEBARS = {
  COMPONENTS: `${WEBPACK__PATHS.HANDLEBARS}/components`,
  HELPERS: `${WEBPACK__PATHS.HANDLEBARS}/helpers`,
  LAYOUTS: `${WEBPACK__PATHS.HANDLEBARS}/layouts`,
  PAGES: `${WEBPACK__PATHS.HANDLEBARS}/pages`,
  DATA: `${WEBPACK__PATHS.HANDLEBARS}/data`
}

/* All paths for js */
export const WEBPACK__PATH__JAVASCRIPT = {
  COMPONENTS: `${WEBPACK__PATHS.JAVASCRIPT}/components`,
  COMMONS: `${WEBPACK__PATHS.JAVASCRIPT}/commons`,
  PAGES: `${WEBPACK__PATHS.JAVASCRIPT}/pages`,
  UTILS: `${WEBPACK__PATHS.JAVASCRIPT}/utils`
}

/* All paths for scss */
export const WEBPACK__PATH__STYLESHEET = {
  COMPONENTS: `${WEBPACK__PATHS.STYLESHEET}/components`,
  CONFIG: `${WEBPACK__PATHS.STYLESHEET}/config`,
  PAGES: `${WEBPACK__PATHS.STYLESHEET}/pages`
}

export const WEBPACK__ENTRIES__NAMES = [
  'contact.page',
  'estimate.page',
  'hireme.page',
  'index.page',
  'lab.page',
  'work.page'
]
