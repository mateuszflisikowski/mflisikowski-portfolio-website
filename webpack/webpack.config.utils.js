import path from 'path'
import {
  WEBPACK__PATH__HANDLEBARS,
  WEBPACK__PATH__JAVASCRIPT,
  WEBPACK__PATH__STYLESHEET
} from './webpack.config.constants'
import { HtmlWebpackPlugin } from './webpack.config.plugins'

export const getHtmlWebpackPluginOptions = (name, chunknames = []) => {
  /* eslint-disable import/no-dynamic-require */
  /* eslint-disable global-require */
  const templateDataPath = path.resolve(__dirname, `${WEBPACK__PATH__HANDLEBARS.DATA}/data.json`)
  const templateData = require(templateDataPath)
  const template = path.resolve(__dirname, `${WEBPACK__PATH__HANDLEBARS.PAGES}/${name}.hbs`)
  const chunks = [`${name}`].concat(chunknames)
  const filename = `${name}.html`

  const obj = {
    filename,
    template,
    chunks
  }

  return {
    templateParameters: { templateData },
    // inject: true,
    minify: false,
    ...obj
  }
}

export const getDevServerRewrites = (entries) => {
  return entries.map((entry) => {
    const entryName = entry.split('.')[0]

    const regExp = {
      // eslint-disable-next-line no-useless-escape
      slug: new RegExp(`^\/${entryName}\$`),
      index: new RegExp(/^\/$/)
    }

    const isIndex = entryName.includes('index')

    return {
      from: isIndex ? regExp.index : regExp.slug,
      to: `/${entry}.html`
    }
  })
}

export const getEntriesObject = (entries) => {
  return entries
    .map((entry) => {
      const scssEntry = path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.PAGES}/${entry}.scss`)
      const jsEntry = path.resolve(__dirname, `${WEBPACK__PATH__JAVASCRIPT.PAGES}/${entry}.js`)

      return {
        [entry]: [scssEntry, jsEntry]
      }
    })
    .reduce((acc, item) => {
      const all = Object.values(item)[0]
      const key = Object.keys(item)[0]
      acc[key] = all
      return acc
    }, {})
}

export const getEntriesArray = (entries) => {
  return entries.map((entry) => {
    const options = getHtmlWebpackPluginOptions(entry)
    return new HtmlWebpackPlugin(options)
  })
}
