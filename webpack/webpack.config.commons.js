import path from 'path'
import {
  WEBPACK__ENTRIES__NAMES,
  WEBPACK__PATH__BUILD,
  WEBPACK__PATH__HANDLEBARS,
  WEBPACK__PATH__JAVASCRIPT,
  WEBPACK__PATH__PUBLIC,
  WEBPACK__PATH__STYLESHEET
} from './webpack.config.constants'
import { CleanWebpackPlugin } from './webpack.config.plugins'
import { getEntriesArray, getEntriesObject } from './webpack.config.utils'

const handlebarsEntries = getEntriesArray(WEBPACK__ENTRIES__NAMES)
const entry = getEntriesObject(WEBPACK__ENTRIES__NAMES)

// console.log(entry)

const output = {
  path: path.resolve(__dirname, WEBPACK__PATH__BUILD),
  publicPath: WEBPACK__PATH__PUBLIC,
  filename: '[name][hash].js'
}

const rules = [
  {
    test: /\.m?js$/,
    exclude: /(node_modules)/,
    use: 'babel-loader'
  },
  {
    test: /\.hbs$/,
    loader: 'handlebars-loader',
    query: {
      helperDirs: [path.resolve(__dirname, WEBPACK__PATH__HANDLEBARS.HELPERS)],
      partialDirs: [
        path.resolve(__dirname, WEBPACK__PATH__HANDLEBARS.COMPONENTS),
        path.resolve(__dirname, WEBPACK__PATH__HANDLEBARS.LAYOUTS)
      ],
      knownHelpersOnly: true,
      exclude: 'node_modules',
      extensions: ['.hbs'],
      debug: false
    }
  }
]

const alias = {
  /* scss aliasses */
  components: path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.COMPONENTS}`),
  typography: path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.CONFIG}/_typography.scss`),
  variables: path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.CONFIG}/_variables.scss`),
  keyframes: path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.CONFIG}/_keyframes.scss`),
  colors: path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.CONFIG}/_colors.scss`),
  mixins: path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.CONFIG}/_mixins.scss`),
  config: path.resolve(__dirname, `${WEBPACK__PATH__STYLESHEET.CONFIG}/_config.scss`),

  /* js aliasses */
  '@components': path.resolve(__dirname, `${WEBPACK__PATH__JAVASCRIPT.COMPONENTS}/components/`),
  '@commons': path.resolve(__dirname, `${WEBPACK__PATH__JAVASCRIPT.COMMONS}/index.js`),
  '@utils': path.resolve(__dirname, `${WEBPACK__PATH__JAVASCRIPT.UTILS}/utils.js`)
}

const extensions = ['.scss', '.css', '.js', '.json']

const plugins = [new CleanWebpackPlugin()].concat(...handlebarsEntries)

export default {
  output,
  entry,
  module: {
    rules
  },
  resolve: {
    extensions,
    alias
  },
  plugins
}
