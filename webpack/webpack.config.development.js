import merge from 'webpack-merge'
import config from './webpack.config.commons'
import { WEBPACK__ENTRIES__NAMES, WEBPACK__PATH__BUILD } from './webpack.config.constants'
import { MiniCssExtractPlugin, TodoWebpackPlugin } from './webpack.config.plugins'
import * as utils from './webpack.config.utils'

const rewrites = utils.getDevServerRewrites(WEBPACK__ENTRIES__NAMES)

const rules = [
  {
    test: /\.(sa|sc|c)ss$/,
    use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader']
  }
]

const plugins = [
  new TodoWebpackPlugin({
    console: true
  }),

  new MiniCssExtractPlugin({
    chunkFilename: '[id].css',
    filename: '[name].css'
  })
]

module.exports = merge(config, {
  mode: 'development',

  devtool: 'inline-source-map',

  stats: 'errors-only',

  devServer: {
    contentBase: WEBPACK__PATH__BUILD,
    historyApiFallback: { rewrites }
  },

  module: { rules },

  plugins
})
