/**
 * Ex. of how to use:
 * {{ stringify htmlWebpackPlugin.options }}
 *
 * @param {*} string
 * @returns
 */
const stringify = (string) => {
  return JSON.stringify(string)
}

export default stringify
