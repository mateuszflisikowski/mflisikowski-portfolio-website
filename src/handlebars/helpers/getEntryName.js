/**
 * Ex. of how to use:
 * {{ getEntryName htmlWebpackPlugin.options.filename }}
 *
 * @param {*} string
 * @returns
 */
const getEntryName = (string) => {
  return string.split('.')[0]
}

export default getEntryName
