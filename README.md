# Mateusz Flisikowski portfolio website

## Getting Started

**Make projects directory and CD into the directory:**

```shell
mkdir ~/projects && cd ~/projects/
```

**Clone the project:**

```shell
git clone git@bitbucket.org:mateuszflisikowski/mflisikowski-portfolio-website.git
```

**CD into the project:**

```shell
cd ~/projects/mflisikowski-portfolio-website/
```

**Install packages:**

```shell
npm install
```

**Start project with dev server:**

```shell
npm start
```

**Build project:**

```shell
npm build
```

## Prerequisites

**Code editor:**

- [Visual Studio Code](https://code.visualstudio.com/)

**Plugins:**

- [Visual Studio Code Commitizen Support](https://marketplace.visualstudio.com/items?itemName=KnisterPeter.vscode-commitizen)
- [EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

## Installing

**Install [NVM](https://github.com/nvm-sh/nvm) manager:**

```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

**Install LTS Node**

```shell
nvm install node
```
